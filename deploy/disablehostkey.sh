# any future command that fails will exit the script
set -e
mkdir -p ~/.ssh
touch ~/.ssh/config
echo "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
