#!/bin/bash

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
# echo $PRIVATE_KEY | tr -d '\r' | ssh-add - > /dev/null
#echo "${PRIVATE_KEY}" | ssh-add -
echo ${PRIVATE_KEY} > /tmp/pk
chmod 400 /tmp/pk
# disable the host key checking.
chmod +x ./deploy/disablehostkey.sh
./deploy/disablehostkey.sh

# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
DEPLOY_SERVERS=$DEPLOY_SERVERS

# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
# ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
ALL_SERVERS=$DEPLOY_SERVERS
echo "ALL_SERVERS ${ALL_SERVERS}"
#server=${ALL_SERVERS}
server=34.227.24.71
# Lets iterate over this array and ssh into each EC2 instance
# Once inside.
# 1. Stop the server
# 2. Take a pull
# 3. Start the server
# for server in "${ALL_SERVERS[@]}";
# do
echo "deploying to ${server}"
#ssh -v ubuntu@${server} 'bash' < ./deploy/updateandrestart.sh
ssh -t -v -o StrictHostKeyChecking=no -i /tmp/pk ubuntu@${server}  'bash' < ./deploy/updateandrestart.sh
# ssh ubuntu@${server} "bash /root/nodejs-on-ec2/deploy/updateandrestart.sh"
# ssh -t ubuntu@${server} 'bash' < "hostname"
# ssh ubuntu@${server} "hostname"
# done
